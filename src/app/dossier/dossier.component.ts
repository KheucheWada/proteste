import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dossier',
  templateUrl: './dossier.component.html',
  styleUrls: ['./dossier.component.css']
})
export class DossierComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  onMar(){
    this.router.navigate(['/prix']);
  }
  retour(){
    this.router.navigate(['/books']);
  }
}
