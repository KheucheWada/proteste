import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dossier2',
  templateUrl: './dossier2.component.html',
  styleUrls: ['./dossier2.component.css']
})
export class Dossier2Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  retour(){
    this.router.navigate(['/books']);
  }
  tour(){
    this.router.navigate(['/dirige']);
  }

}
