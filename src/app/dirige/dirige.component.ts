import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dirige',
  templateUrl: './dirige.component.html',
  styleUrls: ['./dirige.component.css']
})
export class DirigeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  moure(){
    this.router.navigate(['/dossier2']);
  }

}
