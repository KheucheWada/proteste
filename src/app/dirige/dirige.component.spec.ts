import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirigeComponent } from './dirige.component';

describe('DirigeComponent', () => {
  let component: DirigeComponent;
  let fixture: ComponentFixture<DirigeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirigeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirigeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
